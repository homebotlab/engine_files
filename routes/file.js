const express = require('express');
const multer  = require('multer')
const upload = multer();

const authMiddleware = require(`${process.env.MIDDLEWARES_PATH}/authentication`);

const file = require('../controllers/file');

const routes = express.Router();

routes.route('/')
  .all(authMiddleware(), upload.any())
  .post(file.upload);

routes.route('/:id')
  .all(authMiddleware())
  .get(file.download)
  .put(file.update)
  .delete(file.delete);

routes.route('/content/:id')
  .get(file.content)

module.exports = routes;
