sap.ui.define([
	"sap/ui/base/Object",
	"sap/m/MessageView",
	'sap/m/MessagePopoverItem',
	"sap/m/Dialog",
	"sap/m/Button",
	"sap/ui/model/json/JSONModel"
], function(UI5Object, MessageView, MessagePopoverItem, Dialog, Button, JSONModel) {
	"use strict";

	return UI5Object.extend("zehs.act.controller.ErrorHandler", {

		/**
		 * Handles application errors by automatically attaching to the model events and displaying errors when needed.
		 * @class
		 * @param {sap.ui.core.UIComponent} oComponent reference to the app's component
		 * @public
		 * @alias zehsm.act.controller.ErrorHandler
		 */
		constructor: function(oComponent) {
			this._oResourceBundle = oComponent.getModel("i18n").getResourceBundle();
			this._oComponent = oComponent;
			this._oModel = oComponent.getModel();
			this._oMessageModel = new JSONModel();

			var oMessageTemplate = new MessagePopoverItem({
				type: '{type}',
				title: '{title}',
				description: '{description}',
				subtitle: '{subtitle}',
				counter: '{counter}'
			});

			var oBackButton;
			
			var oMessageView = new MessageView({
					showDetailsPageHeader: false,
					// itemSelect: function () {
						//oBackButton.setVisible(true);
					// },
					items: {
						path: "/",
						template: oMessageTemplate
					}
				});
				
			oMessageView.setModel(this._oMessageModel);
				
			this._Dialog = new Dialog({
				title: this._oResourceBundle.getText("errorTitle"),
				resizable: true,
				content: oMessageView,
				beginButton: new sap.m.Button({
					press: function () {
						this.getParent().close();
					},
					text: "Закрыть"
				}),
				// customHeader: new sap.m.Bar({
				// 	contentMiddle: [
				// 		new sap.m.Text({ text: this._oResourceBundle.getText("errorTitle")})
				// 	],
				// 	contentLeft: [oBackButton]
				// }),
				contentHeight: "300px",
				contentWidth: "500px",
				verticalScrolling: false
			});
			
			this._oModel.attachMetadataFailed(function(oEvent) {
				var oParams = oEvent.getParameters();
				this._showServiceError(oParams.response);
			}, this);

			this._oModel.attachRequestFailed(function(oEvent) {
				var oParams = oEvent.getParameters();
				// An entity that was not found in the service is also throwing a 404 error in oData.
				// We already cover this case with a notFound target so we skip it here.
				// A request that cannot be sent to the server is a technical error that we have to handle though
				if (oParams.response.statusCode !== "404" || (oParams.response.statusCode === 404 && oParams.response.responseText.indexOf(
						"Cannot POST") === 0)) {
					this._showServiceError(oParams.response);
				}
			}, this);
		},

		/**
		 * Shows a {@link sap.m.MessageBox} when a service call has failed.
		 * Only the first error message will be display.
		 * @param {string} sDetails a technical error to be displayed on request
		 * @private
		 */
		_showServiceError: function(response) {
			//if (response.statusCode == 400) {
			var responseText = JSON.parse(response.responseText);
			var messages;
			
			messages = [{
						type: "Error", //oItem.severity,
						title: responseText.error.message.value,
						description: responseText.error.message.value,
						subtitle: responseText.error.code,
						counter: 1
			}];
			
			if(responseText.error.innererror.errordetails){
				responseText.error.innererror.errordetails.forEach(function(oItem) {
					messages.push({
						type: "Error", //oItem.severity,
						title: oItem.message,
						description: oItem.message,
						subtitle: oItem.code,
						counter: 1
					});
				});
			}
			this._oMessageModel.setProperty("/", messages);
			this._Dialog.open();
			//}
		},
		
		showHeadersMessages: function(oHeaders){
			if(oHeaders.severity === "error"){
				this._Dialog.setTitle("Ошибка");
				this._Dialog.setState('Error');
			}else{
				this._Dialog.setTitle("Успешная загрузка");
				this._Dialog.setState('None');
			}
			
			var messages = [{
				type: oHeaders.severity === "error" ?  "Error" : "Information",
				title: oHeaders.message,
				description: oHeaders.message,
				subtitle: oHeaders.code,
				counter: 1	
			}];
			
			oHeaders.details.forEach(function(oItem) {
					messages.push({
						type: oHeaders.severity === "error" ?  "Error" : "Information",
						title: oItem.message,
						description: oItem.message,
						subtitle: oItem.code,
						counter: 1
					});
				});
				
			this._oMessageModel.setProperty("/", messages);
			this._Dialog.open();
		}
	});
});