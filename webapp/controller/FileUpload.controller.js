sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/model/json/JSONModel"
], function(Controller, MessageToast, JSONModel) {
	"use strict";

	return Controller.extend("zehs.intgr.imp.controller.FileUpload", {

		onInit: function() {
			// Model used to manipulate control states. The chosen values make sure,
			// detail page is busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data
			var iOriginalBusyDelay,
				oViewModel = new JSONModel({
					busy: true,
					delay: 0,
					LocationUUID: "",
					FileType: ""
				});

			// Store original busy indicator delay, so it can be restored later on
			iOriginalBusyDelay = this.getView().getBusyIndicatorDelay();
			this.getView().setModel(oViewModel, "objectView");
			this.getOwnerComponent().getModel().metadataLoaded().then(function() {
				// Restore original busy indicator delay for the object view
				oViewModel.setProperty("/delay", iOriginalBusyDelay);
			});
		},
		
		onAfterRendering: function(){
			this._setXcsrftoken(this.getView().byId("fileUploader"));
		},
		
		/**
         * Установка X-CSRF-Token
         * @param {Object} oFileUpLoader Загрузчик файлов
         */
        _setXcsrftoken: function(oUploader) {
            var oModel = this.getView().getModel();
			var crsToken = oModel.getSecurityToken();
			
			oUploader.destroyHeaderParameters();
			oUploader.addHeaderParameter(new sap.ui.unified.FileUploaderParameter({name: "x-csrf-token", value: crsToken}));
			oUploader.addHeaderParameter(new sap.ui.unified.FileUploaderParameter({name: "Accept", value: "application/json"}));
        },
		
        /**
         * Загрузка файла на сервер
         */
        handleUploadPress: function() {
        	var oOdataModel = this.getView().getModel();
        	var oObjectViewModel = this.getView().getModel("objectView");
            var oUploader = this.getView().byId("fileUploader");
			if (!oUploader.getValue()) {
				MessageToast.show("Выберите файл");
				return;
			}
			
			var FileType = oObjectViewModel.getProperty("/FileType");
			var LocationUUID = oObjectViewModel.getProperty("/LocationUUID");
			var uploadURL = oOdataModel.sServiceUrl + "/FileCollection?FileType=" + FileType + "&Location=" + LocationUUID;
			oUploader.setUploadUrl(uploadURL);
			
        	oUploader.upload();
        },
        
		handleUploadComplete: function(oEvent) {
			var sHeaders = oEvent.getParameter("headers");
			if(sHeaders["sap-message"]){
				this.getOwnerComponent().showHeadersMessages(
					 JSON.parse(sHeaders["sap-message"])
				);
			}
		},
									
        /**
         * Вывод сообщения о неудачной загрузке
         * @param  {Object} oEvent Загрузка файла через FileUploader
         */
        handleUploadAborted: function(oEvent) {
          //// response header
          //var hdrMessage = response.headers["sap-message"];
          //var hdrMessageObject = JSON.parse(hdrMessage);
          //// log the header message
          //console.log(hdrMessageObject);
          //console.log(hdrMessageObject.message);
          
    		//  var oHeaders = oEvent.getParameter("headers")
    		// if(oHeaders && oHeaders["sap-message"]){
    		// 	JSON.parse(oEvent.getParameter("headers")["sap-message"])
    		// }
          
        },
        
		handleTypeMissmatch: function(oEvent) {
			var aFileTypes = oEvent.getSource().getFileType();
			jQuery.each(aFileTypes, function(key, value) {aFileTypes[key] = "*." +  value;});
			var sSupportedFileTypes = aFileTypes.join(", ");
			MessageToast.show("The file type *." + oEvent.getParameter("fileType") +
									" is not supported. Choose one of the following types: " +
									sSupportedFileTypes);
		},

		handleValueChange: function(oEvent) {
			// MessageToast.show("Press 'Upload File' to upload file '" +
			// 						oEvent.getParameter("newValue") + "'");
		}
	});
});