sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"zehs/intgr/imp/model/models",
	"zehs/intgr/imp/controller/ErrorHandler"
], function(UIComponent, Device, models, ErrorHandler) {
	"use strict";

	return UIComponent.extend("zehs.intgr.imp.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// initialize the error handler with the component
			this._oErrorHandler = new ErrorHandler(this);
				
			// set the device model
			this.setModel(models.createDeviceModel(), "device");
		},
		
		showHeadersMessages: function(oHeaderSapMessages){
			this._oErrorHandler.showHeadersMessages(oHeaderSapMessages);
		}
	});
});