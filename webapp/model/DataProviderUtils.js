sap.ui.define([
], function() {
  'use strict';

  return {
  	getFilesFromResponse: function(sResponse){
		const files = JSON.parse(/<pre .*>(.*)<\/pre>/.exec(sResponse)[1]);
		return files;
  	}

  }
});
