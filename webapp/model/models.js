sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function(JSONModel, Device) {
	"use strict";

	return {

		createDeviceModel: function() {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		removeFile: function(id) {
			return jQuery.ajax({
			  url: `/api/v0.1/files/${id}`,
			  method: 'DELETE',
			  contentType: "application/json"
			})
		}

	};
});