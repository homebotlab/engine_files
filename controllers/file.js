const mongoose = require('mongoose');
const response = require(`${process.env.HELPERS_PATH}/response`);
const acl = require(`${process.env.HELPERS_PATH}/acl`);
const log = require('log4js').getLogger("file");

require('../models/file');
const File = mongoose.model('File');

exports.download = async function (req, res) {
  try {
    const user = req.user;
    const fileId = req.params.id;
    const file = await File.findById(fileId).exec();

    if (!file) return response.sendNotFound(res);

    const role = req.user.role;
    const permission = (user._id == file.owner._id) ?
      acl().can(role).readOwn('file') :
      acl().can(role).readAny('file');

    if (permission.granted) {
      const filteredData = permission.filter(JSON.parse(JSON.stringify(file)));
      return res.json(filteredData);
    }

    response.sendForbidden(res);

  } catch (err) {
    log.error("Download file error", err);
    response.sendBadRequest(res, err);
  }
};

exports.content = async function (req, res) {
  try {
    const user = req.user;
    const fileId = req.params.id;
    const file = await File.findById(fileId).exec();

    if (!file) return response.sendNotFound(res);

    const role = req.user.role;
    const permission = (user._id == file.owner._id) ?
      acl().can(role).readOwn('file') :
      acl().can(role).readAny('file');

    if (permission.granted) {
      const img = new Buffer.alloc(file.file.data.length, file.file.data, 'base64');

      res.attachment(file.file.originalname);
      res.writeHead(200, {
        'Content-Type': file.file.mimetype,
        'Content-Length': img.length
      });
      return res.end(img);
    }

    response.sendForbidden(res);
  
  } catch (err) {
    log.error("Download content file error", err);
    response.sendBadRequest(res, err);
  }
};

exports.upload = async function (req, res) {
  try {
    const { ...data } = req.body;
    const user = req.user;

    const role = user.role;
    const permission = acl().can(role).createOwn('file');

    if (permission.granted) {
      const files = req.files;

      const savingFilePromises = files.map(file => {
        const { originalname, mimetype, buffer } = file;
        const filteredData = permission.filter({
          ...data,
          file: {
            originalname,
            mimetype,
            data: buffer
          }
        });
  
        const newFile = new File({
          ...filteredData,
          owner: user._id
        });
  
        return new Promise((resolve, reject) => {
          const permission = acl().can(role).readOwn('file');
          const filteredData = permission.filter(JSON.parse(JSON.stringify(newFile)));

          newFile.save((err) => {
            if (err) {
              log.error('File upload error', err);
              reject(err)
            } else {
              log.info(`User ${user.email} uploaded file successfully`);
              resolve(filteredData);
            }
          });
        });
      });
      return res.json(await Promise.all(savingFilePromises));
    }
    response.sendForbidden(res);
    
  } catch (err) {
    response.sendBadRequest(res, err);
  }
};

exports.update = async function (req, res) {
  try {
    const {
      ...data
    } = req.body;
    const user = req.user;
    const fileId = req.params.id;
    const file = await File.findById(fileId).exec();

    const role = user.role;
    const permission = (user._id == file.owner._id) ?
      acl().can(role).updateOwn('file') :
      acl().can(role).updateAny('file');

    if (permission.granted) {
      const filteredData = permission.filter({
        ...data
      });
      const updatedFile = await File.findByIdAndUpdate(_id, filteredData, {
        new: true
      }).populate("owner");

      const readPermission = acl().can(role).readOwn('file');
      const filteredData = readPermission.filter(JSON.parse(JSON.stringify(updatedFile)));
      log.info(`User ${user.email} updated file successfully`);

      return filteredData;
    }

    response.sendForbidden(res);
      
  } catch (err) {
    log.error('Error updating file', err);
    response.sendBadRequest(res, err);
  }
};

exports.delete = async function (req, res) {
  try {
    const user = req.user;
    const fileId = req.params.id;
    const file = await File.findById(fileId).exec();

    if (!file) return response.sendNotFound(res);

    const role = user.role;
    const permission = (user._id == file.owner._id) ?
      acl().can(role).deleteOwn('file') :
      acl().can(role).deleteAny('file');

    if (permission.granted) {
      await File.findByIdAndRemove(fileId).exec();
      log.info(`User ${user.email} removed file successfully`);
      return res.json({
        message: 'File successfully deleted'
      });
    }

    response.sendForbidden(res);
      
  } catch (err) {
    log.error('Error updating file', err);
    response.sendBadRequest(res, err);
  }
};