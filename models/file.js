const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema;
const FileSchema = new Schema({
  owner: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  ownerType: {
    type: String,
    enum: ['user'],
    default: 'user'
  },
  fileType: {
    type: String,
    enum: ['photo'],
    default: 'photo'
  },
  file: {
    originalname: String,
    mimetype: String,
    data: Buffer
  },
  date_added: Date
}, {
  toJSON: {
    virtuals: true
  }
});

['find', 'findOne', 'findOneAndUpdate'].forEach(function(hook) {
  FileSchema.pre(hook, function () {
    this.populate('owner');
  });
});

FileSchema.pre('save', function (next) {
  if (!this.date_added) this.date_added = new Date();
  next();
});

FileSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('File', FileSchema);